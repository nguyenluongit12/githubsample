package PageObject;

import org.openqa.selenium.WebDriver;

import ObjectSelector.Selector;
import Utility.WebSupport;

public class GithubLogin {
	private WebDriver driver;

	public GithubLogin(WebDriver driver) {
		this.driver = driver;
	}

	public GithubHomePage loginWith(String email, String password) {

		WebSupport webSupport = new WebSupport(driver);
		
		// Click on Sign in to navigate to Login page
		webSupport.clickOnElement(Selector.signInUrl);

		// Enter email
		webSupport.sendKeysToElement(Selector.loginUsernameTxt, email);

		// Enter password
		webSupport.sendKeysToElement(Selector.loginPasswordTxt, password);

		// Click on Login button
		webSupport.clickOnElement(Selector.signInBtn);
		
		// Return home page
        return new GithubHomePage(driver);
	}
}
