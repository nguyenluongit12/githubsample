package Cases;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import PageObject.GithubLogin;
import Utility.DriverFactory;
import PageObject.GithubHomePage;
import DataFactory.DataFactory;

/* Should login successfully */
public class TC001 {

	public static final Logger logger = LogManager.getLogger("TC001");

	@Test
	public void main() throws Exception {

		logger.info("Should login successfully");
		
		WebDriver driver = DriverFactory.createDriver();

		// Access to Github homepage
		driver.get(DataFactory.homepageUrl);
		logger.info("Access to Github homepage");
		
		// Maximize the browser
		driver.manage().window().maximize();

		// Login to Github
		GithubLogin githubLogin = new GithubLogin(driver);
		GithubHomePage githubHomePage = githubLogin.loginWith(DataFactory.username, DataFactory.password);
		logger.info("Login to Github");

		// Assert
		if (githubHomePage.isDisplayed()) {
			logger.info("Login successfully");
		} else {
			logger.info("Login fail");
		}

		Thread.sleep(3000);

		// Quit driver
		driver.quit();

	}

}
