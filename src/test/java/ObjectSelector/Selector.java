package ObjectSelector;

public class Selector {
	
	/* Home page's elements */
	public static String signInUrl = "/html/body/div[1]/header/div/div[2]/div[2]/a[1]";
	
	/* Login page's elements */
	public static String loginUsernameTxt = "//*[@id=\"login_field\"]";
	public static String loginPasswordTxt = "//*[@id=\"password\"]";
	public static String signInBtn = "//*[@id=\"login\"]/form/div[3]/input[7]";
	public static String loginErrorTxt = "//*[@id=\"js-flash-container\"]";
}
