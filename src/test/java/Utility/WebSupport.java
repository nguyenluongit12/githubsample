package Utility;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebSupport {

	WebDriver driver;
	WebDriverWait wait;
	Actions act;

	public WebSupport(WebDriver driver123) {
		this.driver = driver123;
		this.wait = new WebDriverWait(this.driver, 300);
		this.act = new Actions(driver123);
	}
	
	public boolean waitForElementDisplay(WebDriver driver, By by, int waitInSecond) {
		for (int i = 0; i < waitInSecond / 2 + 1; i++) {
			try {
				if (driver.findElement(by).isDisplayed()) {
					return true;
				}
				Thread.sleep(2 * 1000);
			} catch (Exception e) {
				System.out.println("waiting element for display...");
			}
		}
		return false;
	}

	public void clickOnElement(String xpath) {
		WebElement elm = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		act.moveToElement(elm).build().perform();
		elm.click();
	}

	public void clickOnEnter(String xpath) {
		WebElement elm = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		act.moveToElement(elm).build().perform();
		elm.sendKeys(Keys.ENTER);
	}

	public void sendKeysToElement(String xpath, String keys) {
		WebElement elm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		act.moveToElement(elm).build().perform();
		elm.clear();
		elm.sendKeys(keys);
	}

	public void sendKeysToElementToEdit(String xpath, String keys) {
		WebElement elm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		act.moveToElement(elm).build().perform();
		elm.clear();
		elm.sendKeys(keys);
	}

	public String GetText(String xpath) {
		WebElement elm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		act.moveToElement(elm).build().perform();
		return elm.getText();
	}

	public Boolean verifyElement(String xpath) {
		Boolean result = false;
		try {
			WebElement elm = driver.findElement(By.xpath(xpath));
			act.moveToElement(elm).build().perform();
			Thread.sleep(1000);
			result = true;
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

}
