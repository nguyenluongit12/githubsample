package PageObject;

import org.openqa.selenium.WebDriver;

import DataFactory.DataFactory;

public class GithubHomePage {
	private WebDriver driver;

	public GithubHomePage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean isDisplayed() {

		String currentUrl = driver.getCurrentUrl();

		if (currentUrl.equals(DataFactory.homepageUrl)) {
			return true;
		} else {
			return false;
		}
	}

}
