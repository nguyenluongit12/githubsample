package Cases;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import PageObject.GithubLogin;
import Utility.DriverFactory;
import Utility.WebSupport;
import PageObject.GithubHomePage;
import DataFactory.DataFactory;
import ObjectSelector.Selector;

/* Should return an expected error 'Incorrect user-name or password.' */
public class TC002 {

	public static final Logger logger = LogManager.getLogger("TC002");

	@Test
	public void main() throws Exception {

		logger.info("Should return an expected error 'Incorrect username or password.'");

		WebDriver driver = DriverFactory.createDriver();

		// Access to Github homepage
		driver.get(DataFactory.homepageUrl);
		logger.info("Access to Github homepage");

		// Maximize the browser
		driver.manage().window().maximize();

		// Login to Github with incorrect user details
		String incorrectPassword = RandomStringUtils.randomAlphabetic(10);
		GithubLogin githubLogin = new GithubLogin(driver);
		GithubHomePage githubHomePage = githubLogin.loginWith(DataFactory.username, incorrectPassword);
		logger.info("Login to Github with incorrect user details");

		// Assert
		WebSupport webSupport = new WebSupport(driver);
		Assert.assertEquals(webSupport.GetText(Selector.loginErrorTxt), DataFactory.loginError);
		logger.info("Error is returned");

		Thread.sleep(3000);

		// Quit driver
		driver.quit();
	}
}
